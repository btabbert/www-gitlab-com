---
layout: job_family_page
title: "Community Operations Roles"
descrription: "Learn more about Community Operations roles, responsibilities, requirements and levels."
---
# Community Operations Manager

The GitLab Community Operations Manager job family is responsible for managing operations for the [Community Relations Team](https://about.gitlab.com/handbook/marketing/community-relations/), including the [community programs' application workflow](https://about.gitlab.com/handbook/marketing/community-relations/community-operations/community-program-applications/), budgeting workflows, and infrastructure for the Community Relations team. They own the [Community Operations handbook](https://about.gitlab.com/handbook/marketing/community-relations/community-operations/).

The Community Operations Manager reports to the [Director of Community Relations](https://about.gitlab.com/job-families/marketing/director-of-community-relations/).

### Responsibilities

- **Support Community Programs.** Support the [GitLab for Education](/solutions/education/), [GitLab for Open Source](/solutions/open-source/), and [GitLab for Startups](/solutions/startups/) programs, as well as any new programs that are created, and the associated [Community Programs' application workflow](/handbook/marketing/community-relations/community-operations/automated-community-programs/). This includes responding to all in-bound applications and requests from program members as we work towards further automating these flows.
- **Implement, maintain, and troubleshoot.** Actively participate in the implementation of, and then lead the maintenance of the [Community Programs workflows](/handbook/marketing/community-relations/community-operations/automated-community-programs/). This includes improving and troubleshooting related processes and working cross-functionally with associated teams in Community Relations, Product, Fulfillment, Sales Operations, and Customer Support.
- **Maintain tool stack.** Maintain the tool stack required for all Community Relations team operations. This includes, but is not limited to: Keyhole, Zendesk, Discourse, SheerID, Coupa, Allocadia, Disqus, and Google Analytics. See the [full tech stack](/handbook/marketing/community-relations/community-operations/#community-operations-tool-stack) list here.
- **Maintain budget.** Establish quarterly budgeting processes. Maintain and lead a regular planning and reporting cadence for the Community Relations team.
- **Improve efficiency.** Maximize the Community Relations team's efficiency, productivity, and performance.
- **Measure effectiveness of community programs.** Partner with all Program Managers on the Community Relations team to measure and report the success and effectiveness of their programs. In doing so, you will also be working closely with the Marketing Operations, and Data and Analytics teams.
- **Maintain Community website page content.** Ensure that we have up-to-date content on our [community website pages](/community/) and liaise with the web team to ensure the design and UX is consistent with the rest of the website.
- **Monitor changes.** GitLab moves very quickly, an aspect of this role is keeping up with all the changes that occur in the product and offerings. This role will keep abreast of changes and make sure that the programs adapt.
- **Use GitLab.** Use GitLab extensively to organize work and collaborate cross-functionally.

### Requirements

- You thrive at developing process improvements and are an excellent critical thinker.
- You share our [values](/handbook/values/), and work in accordance with those values.
- You thrive at developing new approaches and refining existing processes to enable teams to work more efficiently.
- You are pattern-seeking and enjoy creating replicable, scalable processes.
- You love making sure community members have the best experience possible as they interact with our tools and systems.
- You excel at working cross-functionally or with multiple stakeholders.
- You have excellent written and spoken English language communication skills.
- You have had experience working in a customer, or community-facing environment, and are able to communicate effectively and empathetically.
- You are very detail oriented.
- You have proven experience creating documentation and process-oriented content.
- You are willing to use GitLab.

Nice-to-haves:
- Familiarity using GitLab, Salesforce, Zendesk, and other tools that the Community Relations team uses.
- Experience creating and maintaining budgets.
- Change management skills.
- Experience updating website copy or making website edits.
- Vendor-management, negotiation, and procurement experience.
- Data-oriented and familiar with defining and implementing key performance metrics.

Note: We strongly encourage people from underrepresented groups to apply. Even if you do not meet 100% of the requirements, we encourage you to apply if you believe you would be a great fit!

### Performance Indicators

- Time to approve community program applications.
- Percentage of manual vs. automated application processes.
- Member/contributor satisfaction for community programs.
* Number of community program applications solved.
* Ability to process a community program application in the time promised our customers; 5-10 business days.

### Hiring Process
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process.
* Qualified candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters.

* Qualified candidates will attend a one 60 minute call with the Community Operations Manager,during which time they can expect to provide a writing sample.

* Qualified candidates will be invited to two or three 45 minute interviews with members of the Community Relations Team. During this time the candidate can expect topics on communication, required skills for the job, and GitLab values.

Additional details about our process can be found on our [hiring page](/handbook/hiring/).


## Job Family Levels
Depending on your experience, you will qualify for one of the following job levels. Each level has its own set of expectations and pay grades.

### Associate Community Operations Manager

The Associate Community Operations Manager reports to the [Director, Community Relations](/job-families/marketing/director-of-community-relations/#director-community-relations).

##### Associate Community Operations Manager Job Grade

The Associate Community Operations Manager is a [grade 5](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

##### Associate Community Operations Manager Responsibilities
All general responsibilities plus the following:

- **Standardize communications for community programs.** As we evolve our community programs, we'll need to make sure that we update all of our outreach and support materials. You will lead this effort and suggest ways we can make our messaging more customer-friendly and effective.  
- **Improve efficiency of tools.** Make sure we are using all of our current tools in the best way possible. Become the team's expert in using each tool and suggest improved ways to use them to help with program goals.

##### Associate Community Operations Manager Requirements

All general requirements plus the following:

- You have a passion for learning and improving.
- You are customer-centric and have a desire to create delightful experiences for our community.

### Community Operations Manager (Intermediate)

The Community Operations Manager (Intermediate) reports to the [Director, Community Relations](/job-families/marketing/director-of-community-relations/#director-community-relations.

##### Community Operations Manager (Intermediate) Job Grade

The Community Operations Manager (Intermediate) is a [grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

##### Community Operations Manager (Intermediate) Responsibilities
Extends the Associate Community Operations Manager responsibilities with the following:
- **Iterate on community program workflows**. Work to improve the community programs application workflow by iterating on what we are already building. Proactively look for ways to make the processes even more efficient.
- **Create policies.** Assist in developing common policies, processes and resources consistent across all community programs, with a handbook-first approach.
- **Assess new tools.** In addition to managing existing tools, you will assess new tools to more effectively serve the GitLab community and to contribute to growth.
- **Dive into metrics.** Partner with all Program Managers on the Community Relations team to measure and report the success of their programs. In doing so, you will also be working closely with the Marketing Operations, and Data and Analytics teams.
- **Establish quarterly budgeting processes.** Create ways to make sure that we are effectively planning for and using our budget. Stay up to date with changes to accounting policies and lead a regular planning and reporting cadence for the Community Relations team.

##### Community Operations Manager (Intermediate) Requirements
 Extends that of the Associate Community Operations Specialist with the following:

- You are extremely proactive.
- You are extremely detail-oriented.
- You are able to learn to use GitLab for all of your project management work.

### Senior Community Operations Manager

The Senior Community Operations Manager reports to the [Director, Community Relations](/job-families/marketing/director-of-community-relations/#director-community-relations).

##### Senior Community Operations Manager Job Grade

The Senior Community Operations Manager is a [grade 7](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

##### Senior Community Operations Manager Responsibilities
Extends the Community Operations Manager (Intermediate) responsibilities with the following:

- **Lead automation processes.** Significantly contribute to the automation all the Community Relations team's processes and workflows.
- **Work cross-functionally.** Proactively work across functions with peers in other groups to ensure collaboration on shared goals.
- **Create frameworks for success.** Collaborate with all Community Relations team's Program Managers to create a framework for community programs, including templates and guidelines for landing pages, contributor events, membership, incentives, etc.
- **Lead updates to community website pages.** You will proactively update community website pages to help make it easier for our community to find the information they need. You'll create processes and workflows to help make sure that the pages stay up-to-date.  
- **Use GitLab extensively for project management and process improvements.** You'll use GitLab to project manage all of your work, and will create best practices to roll out to the rest of the Community Relations team.

##### Senior Community Operations Manager Requirements
Extends that of the Community Operations Manager (Intermediate) requirements with the following:

- You have proven leadership abilities and the ability to influence without authority.
- You have 5+ years in project management, program management, or similar role.
- You have experience leading large or complex, cross-functional projects from start to finish.
- You have an analytical mindset and are experienced with measuring the success of projects and programs.
- You have rolled out new policies across an organization, or team, with great success.

### Career Ladder

The next step for the Community Operations Manager job family is not yet defined past Senior Community Operations Manager.
