---
layout: handbook-page-toc
title: "Security Awards Leaderboard FY23"
description: "Security Awards Leaderboard for Financial Year 2023"
---

### On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

This page is [auto-generated and updated every Mondays](../security-awards-program.html#process).

# Leaderboard FY23

## Yearly

### Development

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@leipert](https://gitlab.com/leipert) | 1 | 2000 |
| [@tkuah](https://gitlab.com/tkuah) | 2 | 1450 |
| [@vitallium](https://gitlab.com/vitallium) | 3 | 1200 |
| [@hfyngvason](https://gitlab.com/hfyngvason) | 4 | 900 |
| [@kassio](https://gitlab.com/kassio) | 5 | 600 |
| [@brodock](https://gitlab.com/brodock) | 6 | 600 |
| [@sethgitlab](https://gitlab.com/sethgitlab) | 7 | 600 |
| [@cam_swords](https://gitlab.com/cam_swords) | 8 | 600 |
| [@.luke](https://gitlab.com/.luke) | 9 | 560 |
| [@Andysoiron](https://gitlab.com/Andysoiron) | 10 | 540 |
| [@ratchade](https://gitlab.com/ratchade) | 11 | 500 |
| [@rossfuhrman](https://gitlab.com/rossfuhrman) | 12 | 500 |
| [@jlear](https://gitlab.com/jlear) | 13 | 500 |
| [@acunskis](https://gitlab.com/acunskis) | 14 | 500 |
| [@peterhegman](https://gitlab.com/peterhegman) | 15 | 500 |
| [@sgoldstein](https://gitlab.com/sgoldstein) | 16 | 500 |
| [@atiwari71](https://gitlab.com/atiwari71) | 17 | 500 |
| [@djadmin](https://gitlab.com/djadmin) | 18 | 500 |
| [@cwoolley-gitlab](https://gitlab.com/cwoolley-gitlab) | 19 | 460 |
| [@garyh](https://gitlab.com/garyh) | 20 | 440 |
| [@jerasmus](https://gitlab.com/jerasmus) | 21 | 440 |
| [@kerrizor](https://gitlab.com/kerrizor) | 22 | 440 |
| [@georgekoltsov](https://gitlab.com/georgekoltsov) | 23 | 400 |
| [@stanhu](https://gitlab.com/stanhu) | 24 | 400 |
| [@xanf](https://gitlab.com/xanf) | 25 | 400 |
| [@toupeira](https://gitlab.com/toupeira) | 26 | 320 |
| [@rcobb](https://gitlab.com/rcobb) | 27 | 300 |
| [@m_frankiewicz](https://gitlab.com/m_frankiewicz) | 28 | 200 |
| [@alexkalderimis](https://gitlab.com/alexkalderimis) | 29 | 150 |
| [@pshutsin](https://gitlab.com/pshutsin) | 30 | 140 |
| [@dskim_gitlab](https://gitlab.com/dskim_gitlab) | 31 | 140 |
| [@furkanayhan](https://gitlab.com/furkanayhan) | 32 | 140 |
| [@10io](https://gitlab.com/10io) | 33 | 130 |
| [@alexpooley](https://gitlab.com/alexpooley) | 34 | 130 |
| [@egrieff](https://gitlab.com/egrieff) | 35 | 120 |
| [@mattkasa](https://gitlab.com/mattkasa) | 36 | 100 |
| [@ifarkas](https://gitlab.com/ifarkas) | 37 | 100 |
| [@sabrams](https://gitlab.com/sabrams) | 38 | 100 |
| [@mwoolf](https://gitlab.com/mwoolf) | 39 | 90 |
| [@digitalmoksha](https://gitlab.com/digitalmoksha) | 40 | 90 |
| [@drew](https://gitlab.com/drew) | 41 | 90 |
| [@dmakovey](https://gitlab.com/dmakovey) | 42 | 80 |
| [@mbobin](https://gitlab.com/mbobin) | 43 | 80 |
| [@rmarshall](https://gitlab.com/rmarshall) | 44 | 80 |
| [@seanarnold](https://gitlab.com/seanarnold) | 45 | 80 |
| [@jprovaznik](https://gitlab.com/jprovaznik) | 46 | 60 |
| [@proglottis](https://gitlab.com/proglottis) | 47 | 60 |
| [@minac](https://gitlab.com/minac) | 48 | 60 |
| [@hortiz5](https://gitlab.com/hortiz5) | 49 | 60 |
| [@pedropombeiro](https://gitlab.com/pedropombeiro) | 50 | 60 |
| [@lauraX](https://gitlab.com/lauraX) | 51 | 60 |
| [@nick.thomas](https://gitlab.com/nick.thomas) | 52 | 60 |
| [@bala.kumar](https://gitlab.com/bala.kumar) | 53 | 60 |
| [@dblessing](https://gitlab.com/dblessing) | 54 | 60 |
| [@cngo](https://gitlab.com/cngo) | 55 | 60 |
| [@avielle](https://gitlab.com/avielle) | 56 | 60 |
| [@allison.browne](https://gitlab.com/allison.browne) | 57 | 60 |
| [@ebaque](https://gitlab.com/ebaque) | 58 | 50 |
| [@ghickey](https://gitlab.com/ghickey) | 59 | 40 |
| [@bdenkovych](https://gitlab.com/bdenkovych) | 60 | 40 |
| [@felipe_artur](https://gitlab.com/felipe_artur) | 61 | 40 |
| [@balasankarc](https://gitlab.com/balasankarc) | 62 | 40 |
| [@ohoral](https://gitlab.com/ohoral) | 63 | 30 |
| [@subashis](https://gitlab.com/subashis) | 64 | 30 |
| [@brytannia](https://gitlab.com/brytannia) | 65 | 30 |
| [@mkaeppler](https://gitlab.com/mkaeppler) | 66 | 30 |
| [@robotmay_gitlab](https://gitlab.com/robotmay_gitlab) | 67 | 30 |
| [@shinya.maeda](https://gitlab.com/shinya.maeda) | 68 | 30 |
| [@ealcantara](https://gitlab.com/ealcantara) | 69 | 30 |
| [@fjsanpedro](https://gitlab.com/fjsanpedro) | 70 | 30 |
| [@ntepluhina](https://gitlab.com/ntepluhina) | 71 | 20 |
| [@mikolaj_wawrzyniak](https://gitlab.com/mikolaj_wawrzyniak) | 72 | 20 |
| [@terrichu](https://gitlab.com/terrichu) | 73 | 20 |

### Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@greg](https://gitlab.com/greg) | 1 | 500 |
| [@godfat-gitlab](https://gitlab.com/godfat-gitlab) | 2 | 500 |
| [@jacobvosmaer-gitlab](https://gitlab.com/jacobvosmaer-gitlab) | 3 | 440 |
| [@f_santos](https://gitlab.com/f_santos) | 4 | 300 |
| [@skarbek](https://gitlab.com/skarbek) | 5 | 300 |
| [@andrewn](https://gitlab.com/andrewn) | 6 | 200 |
| [@katiemacoy](https://gitlab.com/katiemacoy) | 7 | 50 |
| [@rspeicher](https://gitlab.com/rspeicher) | 8 | 30 |
| [@john.mcdonnell](https://gitlab.com/john.mcdonnell) | 9 | 30 |

### Non-Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@NicoleSchwartz](https://gitlab.com/NicoleSchwartz) | 1 | 400 |
| [@mbruemmer](https://gitlab.com/mbruemmer) | 2 | 400 |
| [@vburton](https://gitlab.com/vburton) | 3 | 30 |

### Community

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@rpadovani](https://gitlab.com/rpadovani) | 1 | 1200 |
| [@feistel](https://gitlab.com/feistel) | 2 | 400 |
| [@spirosoik](https://gitlab.com/spirosoik) | 3 | 300 |
| [@benyanke](https://gitlab.com/benyanke) | 4 | 200 |
| [@zined](https://gitlab.com/zined) | 5 | 200 |
| [@trakos](https://gitlab.com/trakos) | 6 | 200 |

## FY23-Q2

### Development

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@furkanayhan](https://gitlab.com/furkanayhan) | 1 | 110 |
| [@sabrams](https://gitlab.com/sabrams) | 2 | 100 |
| [@drew](https://gitlab.com/drew) | 3 | 90 |
| [@dskim_gitlab](https://gitlab.com/dskim_gitlab) | 4 | 80 |
| [@toupeira](https://gitlab.com/toupeira) | 5 | 80 |
| [@seanarnold](https://gitlab.com/seanarnold) | 6 | 80 |
| [@digitalmoksha](https://gitlab.com/digitalmoksha) | 7 | 60 |
| [@egrieff](https://gitlab.com/egrieff) | 8 | 60 |
| [@avielle](https://gitlab.com/avielle) | 9 | 60 |
| [@allison.browne](https://gitlab.com/allison.browne) | 10 | 60 |
| [@jprovaznik](https://gitlab.com/jprovaznik) | 11 | 40 |
| [@kerrizor](https://gitlab.com/kerrizor) | 12 | 40 |
| [@alexpooley](https://gitlab.com/alexpooley) | 13 | 30 |
| [@mwoolf](https://gitlab.com/mwoolf) | 14 | 30 |
| [@mbobin](https://gitlab.com/mbobin) | 15 | 30 |
| [@fjsanpedro](https://gitlab.com/fjsanpedro) | 16 | 30 |
| [@10io](https://gitlab.com/10io) | 17 | 30 |
| [@terrichu](https://gitlab.com/terrichu) | 18 | 20 |

### Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@godfat-gitlab](https://gitlab.com/godfat-gitlab) | 1 | 500 |
| [@jacobvosmaer-gitlab](https://gitlab.com/jacobvosmaer-gitlab) | 2 | 40 |

### Non-Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@mbruemmer](https://gitlab.com/mbruemmer) | 1 | 400 |

### Community

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@trakos](https://gitlab.com/trakos) | 1 | 200 |

## FY23-Q1

### Development

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@leipert](https://gitlab.com/leipert) | 1 | 2000 |
| [@tkuah](https://gitlab.com/tkuah) | 2 | 1450 |
| [@vitallium](https://gitlab.com/vitallium) | 3 | 1200 |
| [@hfyngvason](https://gitlab.com/hfyngvason) | 4 | 900 |
| [@kassio](https://gitlab.com/kassio) | 5 | 600 |
| [@brodock](https://gitlab.com/brodock) | 6 | 600 |
| [@sethgitlab](https://gitlab.com/sethgitlab) | 7 | 600 |
| [@cam_swords](https://gitlab.com/cam_swords) | 8 | 600 |
| [@.luke](https://gitlab.com/.luke) | 9 | 560 |
| [@Andysoiron](https://gitlab.com/Andysoiron) | 10 | 540 |
| [@ratchade](https://gitlab.com/ratchade) | 11 | 500 |
| [@rossfuhrman](https://gitlab.com/rossfuhrman) | 12 | 500 |
| [@jlear](https://gitlab.com/jlear) | 13 | 500 |
| [@acunskis](https://gitlab.com/acunskis) | 14 | 500 |
| [@peterhegman](https://gitlab.com/peterhegman) | 15 | 500 |
| [@sgoldstein](https://gitlab.com/sgoldstein) | 16 | 500 |
| [@atiwari71](https://gitlab.com/atiwari71) | 17 | 500 |
| [@djadmin](https://gitlab.com/djadmin) | 18 | 500 |
| [@cwoolley-gitlab](https://gitlab.com/cwoolley-gitlab) | 19 | 460 |
| [@garyh](https://gitlab.com/garyh) | 20 | 440 |
| [@jerasmus](https://gitlab.com/jerasmus) | 21 | 440 |
| [@georgekoltsov](https://gitlab.com/georgekoltsov) | 22 | 400 |
| [@stanhu](https://gitlab.com/stanhu) | 23 | 400 |
| [@kerrizor](https://gitlab.com/kerrizor) | 24 | 400 |
| [@xanf](https://gitlab.com/xanf) | 25 | 400 |
| [@rcobb](https://gitlab.com/rcobb) | 26 | 300 |
| [@toupeira](https://gitlab.com/toupeira) | 27 | 240 |
| [@m_frankiewicz](https://gitlab.com/m_frankiewicz) | 28 | 200 |
| [@alexkalderimis](https://gitlab.com/alexkalderimis) | 29 | 150 |
| [@pshutsin](https://gitlab.com/pshutsin) | 30 | 140 |
| [@10io](https://gitlab.com/10io) | 31 | 100 |
| [@mattkasa](https://gitlab.com/mattkasa) | 32 | 100 |
| [@alexpooley](https://gitlab.com/alexpooley) | 33 | 100 |
| [@ifarkas](https://gitlab.com/ifarkas) | 34 | 100 |
| [@dmakovey](https://gitlab.com/dmakovey) | 35 | 80 |
| [@rmarshall](https://gitlab.com/rmarshall) | 36 | 80 |
| [@mwoolf](https://gitlab.com/mwoolf) | 37 | 60 |
| [@dskim_gitlab](https://gitlab.com/dskim_gitlab) | 38 | 60 |
| [@proglottis](https://gitlab.com/proglottis) | 39 | 60 |
| [@minac](https://gitlab.com/minac) | 40 | 60 |
| [@egrieff](https://gitlab.com/egrieff) | 41 | 60 |
| [@hortiz5](https://gitlab.com/hortiz5) | 42 | 60 |
| [@pedropombeiro](https://gitlab.com/pedropombeiro) | 43 | 60 |
| [@lauraX](https://gitlab.com/lauraX) | 44 | 60 |
| [@nick.thomas](https://gitlab.com/nick.thomas) | 45 | 60 |
| [@bala.kumar](https://gitlab.com/bala.kumar) | 46 | 60 |
| [@dblessing](https://gitlab.com/dblessing) | 47 | 60 |
| [@cngo](https://gitlab.com/cngo) | 48 | 60 |
| [@ebaque](https://gitlab.com/ebaque) | 49 | 50 |
| [@mbobin](https://gitlab.com/mbobin) | 50 | 50 |
| [@ghickey](https://gitlab.com/ghickey) | 51 | 40 |
| [@bdenkovych](https://gitlab.com/bdenkovych) | 52 | 40 |
| [@felipe_artur](https://gitlab.com/felipe_artur) | 53 | 40 |
| [@balasankarc](https://gitlab.com/balasankarc) | 54 | 40 |
| [@ohoral](https://gitlab.com/ohoral) | 55 | 30 |
| [@subashis](https://gitlab.com/subashis) | 56 | 30 |
| [@brytannia](https://gitlab.com/brytannia) | 57 | 30 |
| [@mkaeppler](https://gitlab.com/mkaeppler) | 58 | 30 |
| [@furkanayhan](https://gitlab.com/furkanayhan) | 59 | 30 |
| [@robotmay_gitlab](https://gitlab.com/robotmay_gitlab) | 60 | 30 |
| [@shinya.maeda](https://gitlab.com/shinya.maeda) | 61 | 30 |
| [@digitalmoksha](https://gitlab.com/digitalmoksha) | 62 | 30 |
| [@ealcantara](https://gitlab.com/ealcantara) | 63 | 30 |
| [@jprovaznik](https://gitlab.com/jprovaznik) | 64 | 20 |
| [@ntepluhina](https://gitlab.com/ntepluhina) | 65 | 20 |
| [@mikolaj_wawrzyniak](https://gitlab.com/mikolaj_wawrzyniak) | 66 | 20 |

### Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@greg](https://gitlab.com/greg) | 1 | 500 |
| [@jacobvosmaer-gitlab](https://gitlab.com/jacobvosmaer-gitlab) | 2 | 400 |
| [@f_santos](https://gitlab.com/f_santos) | 3 | 300 |
| [@skarbek](https://gitlab.com/skarbek) | 4 | 300 |
| [@andrewn](https://gitlab.com/andrewn) | 5 | 200 |
| [@katiemacoy](https://gitlab.com/katiemacoy) | 6 | 50 |
| [@rspeicher](https://gitlab.com/rspeicher) | 7 | 30 |
| [@john.mcdonnell](https://gitlab.com/john.mcdonnell) | 8 | 30 |

### Non-Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@NicoleSchwartz](https://gitlab.com/NicoleSchwartz) | 1 | 400 |
| [@vburton](https://gitlab.com/vburton) | 2 | 30 |

### Community

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@rpadovani](https://gitlab.com/rpadovani) | 1 | 1200 |
| [@feistel](https://gitlab.com/feistel) | 2 | 400 |
| [@spirosoik](https://gitlab.com/spirosoik) | 3 | 300 |
| [@benyanke](https://gitlab.com/benyanke) | 4 | 200 |
| [@zined](https://gitlab.com/zined) | 5 | 200 |


