---
layout: handbook-page-toc
title: MLOps Single-Engineer Group
---

## Updates
{:.no_toc .hidden-md .hidden-lg}

### [11/May/2002] Usage and Progress

Maintenance draining the capacity for new feature exploration, we see accelerated growth on comments for Jupyter Notebook files after the release of 14.5, matching qualitative feedback.

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/ZJawbVesH9g" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

### Past

- 22/Apr/2022 - https://www.youtube.com/embed/F25h6ZYFmWg - We see a correlation between the release of 14.5 (that included the first version on Jupyter Notebook Diffs) and an increase on comments on Notebooks that sustained through the next months, and we are introducing a new backlog for ideas on MLOps @ GitLab.
- 08/Apr/2022 - https://www.youtube.com/embed/qOY09Omnmtk - Toggleable diff modes for Jupyter being released on 14.10, number of comments on Jupyter Notebooks saw a sharp increase this year, a small signals that users are interested on integrating MLFLow into GitLab and using GitLab runners for ML workloads.
- 30/Mar/2022 - https://www.youtube.com/embed/_6ljKL6kCRc - Pipelines as part of Create?: High level overview on how pipelines become part of the CREATE stage for MLOps, potential ways we can deliver value to users, and explorations we have been working on.
- 10/Mar/2022 - https://youtu.be/kBMJKvmqQG4 - General Updates and Introduction to `Pipelines With Stubbed Jobs`, our next thing after Notebook Diffs

[Subscribe to the issue for updates](https://gitlab.com/gitlab-org/incubation-engineering/mlops/meta/-/issues/16)

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## MLOps Single-Engineer Group

DRI: [@eduardobonet](https://gitlab.com/eduardobonet)

MLOps is a [Single-Engineer Group](/company/team/structure/#single-engineer-groups) within our [Incubation Engineering Department](/handbook/engineering/incubation/). This group works on early feature exploration and validation related to the [MLOps group](/direction/modelops/mlops) within the [ModelOps stage](/direction/modelops/).

### Vision

Make GitLab a tool Data Scientists and Machine Learning Engineers love to use.

### Mission

Identify opportunities in our portfolio to explore ways where GitLab can provide a better user experience for Data Science and Machine Learning across the entire Machine Learning life cycle (model creation, testing, deployment, monitoring, and iteration).

### Backlog

https://gitlab.com/groups/gitlab-org/incubation-engineering/mlops/-/epics/8

### Current Exploration Areas

#### User Personas

Issue: https://gitlab.com/gitlab-org/incubation-engineering/mlops/meta/-/issues/40

Data Scientists and Machine Learning Engineers were not considered part of the target audience of GitLab, and that is evidenced by the lack of User Personas and User Research on these groups. Creating these is essencial to create awareness within the company that these users do indeed exist.

#### Improve User Experience for Jupyter on Code Review

Epic: https://gitlab.com/groups/gitlab-org/-/epics/7194

**Why**

While we delivered a [cleaner experience for Jupyter Diffs](https://gitlab.com/groups/gitlab-org/-/epics/6589), which was really well received by our users, the experience is still very limited. Images within notebooks carry a lot of context, reducing the aplicability of the diffs. Additionally, the raw diff is still imporant in some use cases.

**The Vision**

We want to deliver an experience where Data Scientists can review a Notebook in its full context: the images, the source file, the code blocks, the markdown.

**The Way**

For this, we need to recreate the diff algorithm. Instead of using the markdown transformation, we will split the json notebook into blocks to diffed, which actually makes the diffing process faster, and rely on the cell ids to see which cells were added or removed. The output of this is a renderable block, that is mapped into a line of the source notebook, allowing us to

- Display images
- Render Markdown with equations/images/etc
- Code Highlight
- Comments on blocks of the rendered version, which are mapped into the source. Comments on the source also displayed in the rendered version
- Toggling between raw and rendered diff

#### Can we use GitLab Pipelines for ML?

Epic: https://gitlab.com/groups/gitlab-org/incubation-engineering/mlops/-/epics/5

**The Problem**

Due to the size of the data, ML practitioners have to relly on running computations on the cloud, often through pipelines. However, adding the infra necessary for those pipelines to run can be a job for a full-time engineer. Given that pipelines already enter the MLOps lifecycle at the CREATE stage, the current workflow of depending on a commit to run a new pipeline with small like changes hinder productivity.

**The Vision** 

A Data Scientist working on a new model can test it using GitLab by simply running

```glyter run --repo=my_repo my_notebook.ipynb```

**The Way**

We might be able to do an improved POC here without even changing GitLab codebase:

1. Create the Parent/Child pipeline defition on 
2. Use GitLab API to upload the notebook into the repository
3. Trigger a Parent pipeline with the GitLab API
4. Parent Pipeline downloads the notebook and transforms it into a valid gitlab-ci file
5. Parent pipeline triggers the child pipeline

#### Analytics Repository

Epic: https://gitlab.com/groups/gitlab-org/incubation-engineering/mlops/-/epics/5

**The Problem**

When companies grow, so do their Data Science team. And with this growth, knowledge of past research done within the organization gets lost. This is further exacerbated considering that most of this knowledge resides within Jupyter Notebooks, and forgotten within some lost git repository. The impact is that the same work is repeated every few years.

**The Vision**

The first thing Data Scientists when they start a new task is open GitLab, and go to a component where all notebooks created previously are easily indexed and searcheable.

An example of a open source tool that implements this is https://github.com/airbnb/knowledge-repo

**The Way**

We can test a POC using GitLab Pages and Glyter: a pipeline that indexes and publishes them, with support for tags and pretty display, for both RMarkdown and Jupyter Notebooks, with the limitations that it can't do anything too interactive or take notebook across different repositories. 

