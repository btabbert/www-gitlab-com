---
layout: handbook-page-toc
title: Home Office Equipment and Supplies
---

{::options parse_block_html="true" /}

<link rel="stylesheet" type="text/css" href="/stylesheets/biztech.css" />

## On this page
{:.no_toc}

- TOC
{:toc}

## Home Office Equipment and Supplies

### <i class="fas fa-bullseye" id="biz-tech-icons"></i> Home Office Items

Prior to your start date, you will [work with GitLab IT to acquire a laptop](https://about.gitlab.com/handbook/business-technology/team-member-enablement/onboarding-access-requests/#laptops). For US team members, along with being able to order your laptop through GitLab, you will also have the option to order several other pieces of office hardware at the same time, such as monitor, keyboard, mouse, charger, webcam, laptop stand, adapter, and ergonomic desk & chair. You are not required to procure these items through GitLab, but, if these items fit your needs, we suggest choosing from our preselected catalogue because this is an easy option and will be billed directly to GitLab.

If you prefer to choose your own office equipment, are not located in the US, and/or cannot find some items that you may need in the catalogue, please reference the [Price guide for common home office items](#price-guide-for-common-home-office-items) at GitLab. It is uncommon to need all of the items listed in the guide. Read [GitLab's guide to a productive home office or remote workspace](/company/culture/all-remote/workspace/), and use your best judgement and buy them as you need them.

#### Expensing hardware and supplies outside of the US

The prices in the table below can serve as reference for team members as you look into purchasing new equipment. Prices might vary per region depending on taxes, tariffs and other factors. When making a purchase above 100 USD, we encourage team members to look into 2 or 3 different providers and compare prices before acquiring new equipment. 

For non-US team members: if the product cost is higher in your location, confirm that an equivalent product in the United States would be close to the guideline price in the table below. In the event there are additional taxes or other fees in your region, please note this cost increase when submitting an expense report to ensure an efficient and speedy approval and reimbursement from our third party accounting team.

#### Expense constraints

1. If the item you want to purchase is not in the [Price guide for common home office items](#price-guide-for-common-home-office-items), it is not reimbursable.
1. If the item you want to purchase is over the amount in the [price guide](#price-guide-for-common-home-office-items), we will allow up to 20% over the price if you have exercised due diligence in finding the best price. If the purchase is over the 20% threshold, you can ONLY expense the price guide amount plus 20%, and then pay the remaining balance personally.

#### Price guide for common home office items

| Item                                             | Price Guide in USD (for purchases in the USA)     | Importance | Why                                                                                                                                                                                                                                                                                 |
|--------------------------------------------------|--------------------------------------|------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Height-adjustable desk                           | $300 - $500                          | 10/10      | We use our desks everyday and need them to be ergonomic and adjustable to our size and needs, whether that is sitting or standing.                                                                                                                                                  |
| Ergonomic chair                                  | $200 - $400                          | 10/10      | We use our desk chairs hours at a time, and need them to be healthy, supportive, and comfortable.                                                                                                                                                                                   |
| Headphones (wired or wireless, with mic ability) | $100                                 | 10/10      | We use our headphones everyday during our meetings to connect with our fellow team members. We need our headphones to be comfortable, functional, and great quality. Please note that we do not recommend AirPods due to audio quality issues and limited battery life. |
|  Large External monitor                                 | $500 - $1,000                        | 10/10      | Finding a monitor that is large & comfortable to use with sharpness is extremely important for our eyes and health. Some team members prefer one or two 16:9 monitors, while others prefer a single (27''+) high density (4k+) monitor. |
| Keyboard                                         | $250                                 | 10/10      | Find a keyboard that works for you and is comfortable for your workflow and hand size.                                                                                                                                                                                              |
| Mouse or Trackpad                                | $80 or $145                          | 10/10      | Find a mouse/trackpad that works for you and is comfortable for your workflow.                                                                                                                                                                                                      |
| Laptop stand                                     | $90                                  | 10/10      | Your eyes and head must look at the same angle as you work and so your laptop must be elevated if you are working with an external monitor.                                                                                                                                         |
| Webcam                                           | $80                                  | 9/10       | If you would like a much better image quality than from the camera in your laptop, a webcam can make video conversation better. For those who interface routinely with clients, leads, and external parties, you may also consider a pricier mirrorless or DSLR camera as a webcam. |
| Portable 15" external monitor                    | $200                                 | 9/10       | You have the freedom to work from any location, and having a portable monitor allows that your workflow does not suffer from being constrained to a single small laptop screen. iPad's do not qualify.                                                                                                     |
| USB-C Adapter                                    | $80                                  | 9/10       | Most MacBooks only have 1 free USB-C port, so an adapter with additional ports is a necessity.                                                                                                                                                                                      |
| HDMI / monitor cable                             | $15                                  | 9/10       | Find a quality cable so that the connection between your laptop and monitor is healthy and secure.                                                                                                                                                                                  |
| Yubikey                                          | $50  | 8/10       | Per our [Security Practices][yubikey], purchasing Yubikey is not mandatory, but is considered as an extra layer of authentication for better security. As per our [recommendation][yubikey-recommendation], you can buy and expense _two_ Yubikeys |
| Monitor Privacy Filter                           | $80                                  | 8/10       | Important if you work in public places and need to be certain your work cannot be seen.                                                                                                                                                                                             |
| WiFi Router with guest functionality/Powerline/WiFi network extender          | $100                               | 7/10       | If your existing router does not allow for isolating your work notebook from your personal devices in your home network, consider buying a router that does.  You can also choose to extend your home network to your workspace by using a network extender.                                                                                                                    |
| Laptop bag or backpack                           | $60                                  | 7/10       | Carry your laptop and external monitor safely with this travel bag. We recommended you get a bag (or backpack) with straps so the device stays on you when you need your hands free.                                                                                                |
| Laptop cooling pad                               | $35 - $50                            | 6/10       | Useful if you live in a place where summer heat can affect your laptop's ability to keep itself cool                                                                                                                                                                                |
| Dedicated microphone                             | $130                                 | 6/10       | For those who routinely interface with clients, leads, media, and external parties — or create regular content for GitLab channels — you may also consider a dedicated microphone to capture your voice with added richness and detail.                                             |
| Ethernet connector                               | $20                                  | 6/10       | This is if you choose to connect to your internet directly versus by Wi-Fi.             
| Office Consumables                                   | $10-$20                                  | 3/10       | To maintain your home office equipment, items like compressed air, pens, etc may be required from time to time. |

[yubikey]: /handbook/tools-and-tips/#u2f-devices
[yubikey-recommendation]: /handbook/tools-and-tips/#recommendations

#### Suggested Items and Examples
  * If you would like to see some examples or suggested items, please reference our [Amazon list](https://a.co/gtDSu2K). These exact items may not fit your needs or may not be available in your location, but can be used as a guideline for options within our suggested price ranges above.

See also the [Laptop Configuration page](/handbook/business-technology/team-member-enablement/onboarding-access-requests/#laptop-configurations) before purchasing.
Please message the `#expense-reporting-inquires` channel if you are unsure what you would like to purchase is reimbursable.

