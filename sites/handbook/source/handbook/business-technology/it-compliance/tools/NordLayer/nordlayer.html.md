---
layout: handbook-page-toc
title: "Nordlayer"
description: "What is NordLayer?"
---
<link rel="stylesheet" type="text/css" href="/stylesheets/biztech.css" />

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

#### What Is NordLayer?
NordLayer is our supported VPN (Virtual Private Network) platform for GitLab Team Members. VPN solutions have numerous benefits for organizational security—it is a technology that provides secure internet connections for remote users accessing company resources. VPNs also establish data encryption that conceals ongoing data transactions. A VPN can encrypt data traffic, separate it from public Wi-Fi, and secure remote access to corporate assets even from unprotected networks.


#### Why NordLayer?
When we first approached the idea of a simple VPN for those times that our employees work away from home (coffee shops, trains, planes, etc.), we found a lot of options out there. While many VPN options exist, some are much more than we need, and some far less. When it comes to our decision on NordLayer, we had a number of things to look at: Is it secure? Is it easy to administer? Does it cover multiple Operating Systems? Are admin actions logged? We tested many options and while a number fit a few of these, NordLayer fit the most with security being our most important criteria. As we look forward with NordLayer, we are hoping to add this to Okta to help with provisioning, and decrease the setup work for you. More details on this as they come.

#### Do I have to use Nordlayer?
The use of Nordlayer is optional for Team Members. However it is recommended for Team Members that often work on guest networks in a public setting. That could be at a coworking location, an airport, a coffee shop or on a guest network at a customers office. Some members may already have sourced their own VPN solution for these scenarios and we would recommend that they move to Nordlayer when convenient.

If you have any questions related to Nordlayer or would like to request a license, please reach out in the #it_compliance_security_help Slack channel.

