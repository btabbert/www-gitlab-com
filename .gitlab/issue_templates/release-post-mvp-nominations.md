<!--
The Release Post Manager uses this issue to collect MVPs for the release post. This issue is created by [release post branch creation task](https://about.gitlab.com/handbook/marketing/blog/release-posts/#release-post-branch-creation-rake-task)
-->

# Overview

1. The purpose of this issue is to collect MVP nominations for this release from the product team, community relations, and MR coaches.
1. During the Release Post kickoff tasks, the Release Post Manager will share a link to this Issue in the #release-post, #community-relations, #mr-coaching and #core Slack channels to encourage GitLab team members to share MVP nominations.
1. On the 12th of the month, the Release Post Manager will encourage GitLab team members to vote on the nominations.
1. On the 15th of the month, the Release Post Manager will choose the nominee with the most votes.

@gl-product @gitlab-org/coaches @gitlab-com/marketing/community-relations

## Community contributions

In case you need some help refreshing your memory, here's a list of popular community contributions:

- [Community contributions in X.Y](https://gitlab.com/gitlab-org/gitlab/-/merge_requests?label_name%5B%5D=Community+contribution&milestone_title=_MILESTONE_&scope=all&sort=popularity&state=merged)

## To add a nominee

- Add a comment to this issue with the contributor name, GitLab profile and some details of their contribution.

## To vote for a nominee

- Add 👍 to place a vote for a nominee

/label ~"Product Operations"
/label ~"release post"
/milestone %_MILESTONE_
